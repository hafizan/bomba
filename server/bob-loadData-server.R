posts <- read.csv("data/clean/bomba_full.csv", stringsAsFactors = FALSE)
#### Declaring Time Zone format for the data ####
posts$created_time <- as.POSIXct(posts$created_time, tz="GMT")

#### Convert to local timezone ####
posts$created_time <- format(posts$created_time, tz = "Asia/Kuala_Lumpur", usetz = TRUE) # will return the data in character

#### Remove MYT words in local timezone ####
posts$created_time <- gsub(" MYT", "", posts$created_time) 

#### Cast to time date format ####
posts$created_time <- as.POSIXct(posts$created_time, tz = "Asia/Kuala_Lumpur", format ="%Y-%m-%d %H:%M:%S")

#### Order by created_time assendingly ####
posts <- posts[order(posts$created_time, decreasing = FALSE),]