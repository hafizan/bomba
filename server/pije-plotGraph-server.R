puncakbkr15[3:15] <- sapply(puncakbkr15[3:15], as.numeric)
puncakbkr15[is.na(puncakbkr15)] <- 0

coord$Negeri <- toupper((coord$Negeri))

states <- stateDensityFn(states, negeriCount, 2015)


#set the color pallete
bins <- c(100, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000)
pal <- colorBin("YlOrRd", domain = states$density, bins = bins)

output$livemap <- renderLeaflet({
  
  m <- leaflet(states) %>%
    setView(108.292236, 3.626360, zoom=6) %>%
    addTiles()
  
  labels <- sprintf(
    "<strong>%s</strong><br/>%g Kes",
    states$Name, states$density
  ) %>% lapply(htmltools::HTML)
  
  m <- m %>% addPolygons(
    fillColor = ~pal(density),
    weight = 2,
    opacity = 1,
    color = "white",
    dashArray = "3",
    fillOpacity = 0.7,
    layerId = states$Name,
    label = labels,
    labelOptions = labelOptions(
      style = list("font-weight" = "normal", padding = "3px 8px"),
      textsize = "15px",
      direction = "auto")) %>%
    addLegend(pal = pal, values = ~density, opacity = 0.7, title = NULL,
              position = "bottomright")%>%
    addMinicharts(
      coord$Longitute, coord$Latitude,
      layerId = coord$Negeri,
      width = 40, height = 40
    )
  
  m
})

observeEvent(input$jenisLaporan, {
  
  jenis <- input$jenisLaporan
  
  data <- puncakbkr15[,-c(1,2)]
  
  if(jenis == "TAKSIRAN"){
    data <- taksiranNegeri15[,-c(1,2)]
  }
  
  maxValue <- max(as.matrix(data))
  
  leafletProxy("livemap", session) %>%
    updateMinicharts(
      puncakbkr15$state,
      chartdata = data,
      maxValues = maxValue,
      type = "pie",
      showLabels = TRUE,
      legendPosition = "bottomleft"
    )
})

#TreeMap
output$barTaksiran <- renderPlotly({
  p <- plot_ly(taksiranNegeri15, x = ~state, y = ~kerugian, type = 'bar', name = 'Kerugian', marker = list(color = "rgb(67, 67, 72)")) %>%
    add_trace(y = ~diselamatkan,  name = 'Diselamatkan', marker = list(color = "rgb(144, 237, 125)")) %>%
    layout(yaxis = list(title = 'Count'), barmode = 'stack')
  
  p
  
})

output$barJenis <- renderPlotly({
  plot_ly(x = jenisKebakaran15$X2015, y = jenisKebakaran15$Jenis, type = 'bar', orientation = 'h', color = jenisKebakaran15$Jenis)
})

output$mapKebakaranTahunan <- renderLeaflet({
  
  m <- leaflet(states) %>%
    setView(108.292236, 3.626360, zoom=6) %>%
    addTiles()
  
  labels <- sprintf(
    "<strong>%s</strong><br/>%g Kes",
    states$Name, states$density
  ) %>% lapply(htmltools::HTML)
  
  m <- m %>% addPolygons(
    fillColor = ~pal(density),
    weight = 2,
    opacity = 1,
    color = "white",
    dashArray = "3",
    fillOpacity = 0.7,
    layerId = states$Name,
    label = labels,
    labelOptions = labelOptions(
      style = list("font-weight" = "normal", padding = "3px 8px"),
      textsize = "15px",
      direction = "auto")) %>%
    addLegend(pal = pal, values = ~density, opacity = 0.7, title = NULL,
              position = "bottomright")
  
  m
})